# Ensi Cloud Api SDK

# Подключение к сервису

Детали разворота и подключения sdk описана [тут](https://docs.ensi.tech/backend-guides/principles/http-client#шаблон-для-sdk-пакетов) 

## Requirements

PHP 8.1 and later

## Installation & Usage

### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/greensight/ensi/catalog/clients/cloud-api-sdk-php.git"
    }
  ],
  "require": {
    "ensi/cloud-api-sdk": "dev-master"
  }
}
```

Then run `composer install`

## Author

mail@greensight.ru

