<?php

namespace Ensi\CloudApiSdk\Dto\Base;

abstract class BaseBodyDto extends BaseRequestDto
{
    public function getBody(): array
    {
        return $this->toArray();
    }
}
