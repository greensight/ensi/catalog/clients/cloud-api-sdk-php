<?php

namespace Ensi\CloudApiSdk\Dto\Base;

abstract class BaseQueryDto extends BaseRequestDto
{
    public function getQueryParams(): array
    {
        return $this->toArray();
    }
}
