<?php

namespace Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts;

use Ensi\CloudApiSdk\Dto\Base\BaseBodyDto;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction\ProductAction;

/**
 * @property ProductAction[] $actions
 */
class IndexesProductsRequest extends BaseBodyDto
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->mapAttributeToArray('actions', ProductAction::class);
    }
}
