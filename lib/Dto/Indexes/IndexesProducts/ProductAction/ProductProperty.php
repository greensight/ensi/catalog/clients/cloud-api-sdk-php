<?php

namespace Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction;

use Ensi\CloudApiSdk\Dto\Base\BaseDto;

/**
 * @property string $name Название
 * @property array $values
 */
class ProductProperty extends BaseDto
{
}
