<?php

namespace Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction;

use Ensi\CloudApiSdk\Dto\Base\BaseDto;

/**
 * @property string $name Название
 * @property string|null $url Ссылка на страницу с товаром
 * @property string[] $category_ids Одна или более идентификаторов категорий, в которые входит товар
 * @property string|null $brand Бренд
 * @property string $vendor_code Артикул
 * @property string[] $barcodes
 * @property string|null $description Описание товара
 * @property string|null $picture Ссылка на картинку с товаром
 * @property string|null $country Страна
 * @property string[] $group_ids
 * @property ProductLocation[] $locations
 * @property ProductProperty[] $properties
 */
class ProductBody extends BaseDto
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->mapAttributeToArray('locations', ProductLocation::class);
        $this->mapAttributeToArray('properties', ProductProperty::class);
    }
}
