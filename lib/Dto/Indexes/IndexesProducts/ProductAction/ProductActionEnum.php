<?php

namespace Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction;

enum ProductActionEnum: string
{
    case CREATE = 'create';
    case UPDATE = 'update';
    case DELETE = 'delete';
}
