<?php

namespace Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction;

use Ensi\CloudApiSdk\Dto\Base\BaseDto;

/**
 * @property ProductActionEnum $action Доступные действия
 * @property string $id ID товара в PIM
 * @property ProductBody|null $body
 */
class ProductAction extends BaseDto
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->mapEnum('action', ProductActionEnum::class);
        $this->mapAttribute('body', ProductBody::class, setDefault: false);
    }
}
