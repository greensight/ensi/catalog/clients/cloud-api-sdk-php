<?php

namespace Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction;

use Ensi\CloudApiSdk\Dto\Base\BaseDto;

/**
 * @property string $id id магазина/склада/местности
 * @property int|null $price Цена на товар (коп.)
 */
class ProductLocation extends BaseDto
{
}
