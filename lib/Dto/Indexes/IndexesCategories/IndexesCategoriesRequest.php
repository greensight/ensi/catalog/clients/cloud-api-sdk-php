<?php

namespace Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories;

use Ensi\CloudApiSdk\Dto\Base\BaseBodyDto;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\CategoryAction\CategoryAction;

/**
 * @property CategoryAction[] $actions
 */
class IndexesCategoriesRequest extends BaseBodyDto
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->mapAttributeToArray('actions', CategoryAction::class);
    }
}
