<?php

namespace Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\CategoryAction;

use Ensi\CloudApiSdk\Dto\Base\BaseDto;

/**
 * @property string $name Название
 * @property string|null $url Ссылка на страницу с категорией
 * @property string[] $parent_ids Нуль или более идентификаторов родительских категорий, включая родителей родителей и т.д.
 */
class CategoryBody extends BaseDto
{
}
