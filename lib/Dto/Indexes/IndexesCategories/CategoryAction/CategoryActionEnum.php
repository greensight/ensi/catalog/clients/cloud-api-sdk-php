<?php

namespace Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\CategoryAction;

enum CategoryActionEnum: string
{
    case CREATE = 'create';
    case UPDATE = 'update';
    case DELETE = 'delete';
}
