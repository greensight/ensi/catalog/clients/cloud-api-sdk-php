<?php

namespace Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\CategoryAction;

use Ensi\CloudApiSdk\Dto\Base\BaseDto;

/**
 * @property CategoryActionEnum $action Доступные действия
 * @property string $id ID товара в PIM
 * @property CategoryBody|null $body
 */
class CategoryAction extends BaseDto
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->mapEnum('action', CategoryActionEnum::class);
        $this->mapAttribute('body', CategoryBody::class, setDefault: false);
    }
}
