<?php

namespace Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Response\Data;

use Ensi\CloudApiSdk\Dto\Base\BaseResponseDto;

/**
 * @property string $word Короткая поисковая подсказка (слово)
 * @property string $hint Целевая поисковая подсказа
 *
 */
class CatalogSearchProductHint extends BaseResponseDto
{
}
