<?php

namespace Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Response\Data;

use Ensi\CloudApiSdk\Dto\Base\BaseResponseDto;

/**
 * @property string[] $products ИД товаров
 * @property int $total_products Общее кол-во найденных товаров
 * @property string[] $categories ИД категорий
 * @property int $total_categories Общее кол-во найденных категорий
 * @property string $correction Исправленный запрос
 * @property CatalogSearchProductHint $product_hints Поисковая подсказка
 *
 */
class CatalogSearchData extends BaseResponseDto
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->mapAttributeToArray('product_hints', CatalogSearchProductHint::class, setDefault: false);
    }
}
