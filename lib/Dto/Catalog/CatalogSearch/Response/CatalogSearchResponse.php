<?php

namespace Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Response;

use Ensi\CloudApiSdk\Dto\Base\BaseResponseDto;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Response\Data\CatalogSearchData;

/**
 * @property CatalogSearchData $data
 */
class CatalogSearchResponse extends BaseResponseDto
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->mapAttribute('data', CatalogSearchData::class);
    }
}
