<?php

namespace Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Filter;

use Ensi\CloudApiSdk\Dto\Base\BaseBodyDto;

/**
 * @property string $name Название свойства
 * @property string[] $values Значения свойства
 */
class CatalogSearchPropertyFilter extends BaseBodyDto
{
}
