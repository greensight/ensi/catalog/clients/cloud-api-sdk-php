<?php

namespace Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Filter;

use Ensi\CloudApiSdk\Dto\Base\BaseBodyDto;

/**
 * @property string $location_id ID магазина/склада/местности
 * @property string $query Поисковый запрос
 * @property string[] $category_ids Идентификаторы категорий
 * @property string[] $brands Название бренда
 * @property string[] $countries Название страны-производителя
 * @property CatalogSearchPropertyFilter[] $properties Фильтрация по свойствам
 */
class CatalogSearchFilter extends BaseBodyDto
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->mapAttributeToArray('properties', CatalogSearchPropertyFilter::class, setDefault: false);
    }
}
