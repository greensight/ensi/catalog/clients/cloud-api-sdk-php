<?php

namespace Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Sort;

enum CatalogSearchSortEnum: string
{
    case RELEVANCE = 'relevance';
    case NAME = 'name';
    case PRICE = 'price';
    case POPULAR = 'popular';
}
