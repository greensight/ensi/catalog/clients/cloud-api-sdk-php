<?php

namespace Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request;

use Ensi\CloudApiSdk\Dto\Base\BaseBodyDto;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Filter\CatalogSearchFilter;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Include\CatalogSearchIncludeEnum;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Pagination\CatalogSearchPagination;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Sort\CatalogSearchSortEnum;

/**
 * @property CatalogSearchFilter $filter
 * @property CatalogSearchPagination $pagination
 * @property CatalogSearchIncludeEnum[] $include Управление ответом
 * @property CatalogSearchSortEnum $sort Способ сортировки
 */
class CatalogSearchRequest extends BaseBodyDto
{
    public ?string $customerId = null;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->mapAttribute('filter', CatalogSearchFilter::class, setDefault: false);
        $this->mapAttribute('pagination', CatalogSearchPagination::class, setDefault: false);
        $this->mapEnumToArray('include', CatalogSearchPagination::class);
        $this->mapEnum('sort', CatalogSearchSortEnum::class, setDefault: false);
    }

    public function getHeaders(): array
    {
        $headers = [];
        if ($this->customerId) {
            $headers['X-Customer-Id'] = $this->customerId;
        }

        return $headers;
    }
}
