<?php

namespace Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Pagination;

use Ensi\CloudApiSdk\Dto\Base\BaseBodyDto;

/**
 * @property int $limit_products Количество позиций товаров
 * @property int $offset_products Смещение товаров
 * @property int $limit_categories Количество позиций категорий
 */
class CatalogSearchPagination extends BaseBodyDto
{
}
