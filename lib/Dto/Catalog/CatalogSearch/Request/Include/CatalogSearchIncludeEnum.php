<?php

namespace Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Include;

enum CatalogSearchIncludeEnum: string
{
    case PRODUCTS = 'products';
    case CATEGORIES = 'categories';
    case CORRECTION = 'correction';
    case PRODUCT_HINTS = 'product_hints';
}
