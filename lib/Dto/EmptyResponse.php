<?php

namespace Ensi\CloudApiSdk\Dto;

use Ensi\CloudApiSdk\Dto\Base\BaseResponseDto;

class EmptyResponse extends BaseResponseDto
{
}
