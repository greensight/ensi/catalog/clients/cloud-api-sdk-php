<?php

namespace Ensi\CloudApiSdk;

class Configuration
{
    private static Configuration $defaultConfiguration;

    protected string $publicToken = '';

    protected string $privateToken = '';

    /** @var string The host */
    protected string $host = 'http://localhost/api/v1';

    /** @var string User agent of the HTTP request, set to "Ensi/1.0.0/PHP" by default */
    protected string $userAgent = 'Ensi/1.0.0/PHP';

    public function setPublicToken(string $publicToken): static
    {
        $this->publicToken = $publicToken;

        return $this;
    }

    public function getPublicToken(): string
    {
        return $this->publicToken;
    }

    public function setPrivateToken(string $privateToken): static
    {
        $this->privateToken = $privateToken;

        return $this;
    }

    public function getPrivateToken(): string
    {
        return $this->privateToken;
    }

    public function setHost(string $host): static
    {
        $this->host = $host;

        return $this;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function setUserAgent(string $userAgent): static
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    public static function getDefaultConfiguration(): Configuration
    {
        if (self::$defaultConfiguration === null) {
            self::$defaultConfiguration = new Configuration();
        }

        return self::$defaultConfiguration;
    }

    public static function setDefaultConfiguration(Configuration $config): void
    {
        self::$defaultConfiguration = $config;
    }
}
