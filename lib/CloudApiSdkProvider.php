<?php

namespace Ensi\CloudApiSdk;

class CloudApiSdkProvider
{
    public static array $apis = [
        '\Ensi\CloudApiSdk\Api\IndexesApi',
        '\Ensi\CloudApiSdk\Api\CatalogApi',
    ];

    public static string $configuration = Configuration::class;
}
