<?php

namespace Ensi\CloudApiSdk\Api;

use Ensi\CloudApiSdk\Dto\EmptyResponse;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesCategories\IndexesCategoriesRequest;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\IndexesProductsRequest;
use Ensi\CloudApiSdk\RequestBuilder;
use GuzzleHttp\Promise\PromiseInterface;

class IndexesApi extends BaseApi
{
    public function products(IndexesProductsRequest $request): EmptyResponse
    {
        return $this->send($this->productsRequest($request), fn ($content) => new EmptyResponse($content));
    }

    public function productsAsync(IndexesProductsRequest $request): PromiseInterface
    {
        return $this->sendAsync($this->productsRequest($request), fn ($content) => new EmptyResponse($content));
    }

    protected function productsRequest(IndexesProductsRequest $requestDto): RequestBuilder
    {
        return (new RequestBuilder('/indexes/products', 'POST'))->private()->json($requestDto);
    }

    public function categories(IndexesCategoriesRequest $request): EmptyResponse
    {
        return $this->send($this->categoriesRequest($request), fn ($content) => new EmptyResponse($content));
    }

    public function categoriesAsync(IndexesCategoriesRequest $request): PromiseInterface
    {
        return $this->sendAsync($this->categoriesRequest($request), fn ($content) => new EmptyResponse($content));
    }

    protected function categoriesRequest(IndexesCategoriesRequest $requestDto): RequestBuilder
    {
        return (new RequestBuilder('/indexes/categories', 'POST'))->private()->json($requestDto);
    }
}
