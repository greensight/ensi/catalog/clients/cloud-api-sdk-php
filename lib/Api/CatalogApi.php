<?php

namespace Ensi\CloudApiSdk\Api;

use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\CatalogSearchRequest;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Response\CatalogSearchResponse;
use Ensi\CloudApiSdk\RequestBuilder;
use GuzzleHttp\Promise\PromiseInterface;

class CatalogApi extends BaseApi
{
    public function search(CatalogSearchRequest $request): CatalogSearchResponse
    {
        return $this->send($this->searchRequest($request), fn ($content) => new CatalogSearchResponse($content));
    }

    public function searchAsync(CatalogSearchRequest $request): PromiseInterface
    {
        return $this->sendAsync($this->searchRequest($request), fn ($content) => new CatalogSearchResponse($content));
    }

    protected function searchRequest(CatalogSearchRequest $requestDto): RequestBuilder
    {
        return (new RequestBuilder('/catalog/search', 'POST'))->public()->json($requestDto);
    }
}
